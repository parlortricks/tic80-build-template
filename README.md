# Build Template for Tic80

## Summary
This repository contains a generic build template for tic80 games to kick start a project. It was designed with the language fennel in mind, but can easily be converted over to lua or something else.

This was developed on Manjaro Linux but should work on any Linux distribution as long as you have all the tools required.

What does the template provide:
 * compile all source
 * run game
 * export to .zip(html),.tic,win,linux,mac and PNG!
 * upload to itch.io
 * format code using fnlfmt

## Tools Required
Here is a list of tools required for this to work:
 * tic80 (only thing that needs Pro so far is alone=1 flag when exporting binaries. Tic80 free will just ignore the variable)
 * fennel from https://fennel-lang.org/
 * butler from https://itchio.itch.io/butler
 * fnlfmt from https://git.sr.ht/~technomancy/fnlfmt

Make sure all the tools are available in your `PATH` variable. Check your Linux distribution on how to do this. In Manjaro i just had to add it to my `.zshrc` file.

## Makefile
 * Compile
   * gathers all the listed source files from `SRC` and compiles it into a single file `out.fnl`. It launches a process that is constantly watching the source files for changes and re-compiles if it sees any.
   * Records the PID to a file `compile.pid` to assist in the clean task later
 * Run
   * launch Tic80 cart and import `out.fnl` and run the game
 * Export
   * zip
     * Launches Tic80 and exports the game to a HTML zip ready for upload to itch.io and places it in the `build` directory
   * cart
     * Launches Tic80 and saves the game to a `.tic` and places it in the `build` directory
   * win,linux,mac
     * export an executable that can be run on the respective operating systems
   * PNG
     * export a PNG cart you can distribute
 * Upload
   * Using butler from itch.io this task uploads the `zip` file created to itch.io. Requires that you have already created an empty project on itch.io
 * Status
   * Check the status of the upload task, it looks at itch.io to report back if its done or not
 * Count
   * Using the command `cloc` it provides stats on your sourcefile such as how many code vs comment lines
 * Clean
   * Remove all the temporary and uneeded files
   * Will kill the compile process that is watching files as well
 * Check
   * Use fennel to perform checking of your code
 * Format
   * Use fnlfmt to format the code

## License
[SPDX-License-Identifier: GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2021 parlortricks