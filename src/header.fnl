;; title:   build-template
;; author:  parlortricks
;; desc:    a build-template to sort out the build process
;; website: https://parlortricks.itch.io/build-template
;; script:  fennel
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright (c) 2021 parlortricks
